import React from 'react';

class Delete extends React.Component {

    state = {
        id: '',
    }
    
    onSubmit(event) {
        event.preventDefault();
    }

    handleSubmit = () => {
        this.props.deletePlanet(this.state.id);
        this.setState({
            id: '',
        })
    }

    render() {
        return (
            <div>
                <form onSubmit={this.onSubmit} className="planetUpdateForm">
                    <div className="form-control">
                        <input type="text" className="form-control" name="planet-id" placeholder="ID of planet" onChange={(e) => this.setState({ id: e.target.value })} value={this.state.id}></input>
                    </div>
                    
                    <button onClick={this.handleSubmit}>Submit</button>
                </form>
            </div>
        )
    }
}
export default Delete;