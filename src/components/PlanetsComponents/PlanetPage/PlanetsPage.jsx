import React from 'react';
import { Link } from 'react-router-dom';
import Styles from './PlanetsPage.module.css';

const PlanetsPage = props => {
    return (
        <div>
            <nav className={Styles.navClass}>
                <ul className={Styles.ulClass}>
                    <li className={Styles.liClass}>
                        <Link to="/planets/all" className={Styles.linkClass}>All</Link>
                    </li>
                    <li className={Styles.liClass}>
                        <Link to="/planets/add" className={Styles.linkClass}>Add</Link>
                    </li>
                    <li className={Styles.liClass}>
                        <Link to="/planets/update" className={Styles.linkClass}>Update</Link>
                    </li>
                    <li className={Styles.liClass}>
                        <Link to="/planets/delete" className={Styles.linkClass}>Delete</Link>
                    </li>
                </ul>
            </nav>
        </div>
    );
}

export default PlanetsPage;