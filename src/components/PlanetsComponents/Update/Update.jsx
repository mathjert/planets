import React from 'react';

class Update extends React.Component {

    state = {
        id: '',
        name: '',
        size: '',
        date_discovered: '',
    }

    onSubmit(event) {
        event.preventDefault();
    }

    handleSubmit = () => {
        this.props.updatePlanet(this.state.id, this.state.name, this.state.size, this.state.date_discovered);
        this.setState({
            name: '',
            size: '',
            date_discovered: '',
        })
    }

    render() {
        return (
            <div>
                <form onSubmit={this.onSubmit} className="planetUpdateForm">
                    <div className="form-control">
                        <input type="text" className="form-control" name="planet-id" placeholder="ID of planet" onChange={(e) => this.setState({ id: e.target.value })} value={this.state.id}></input>
                    </div>
                    <div className="form-control">
                        <input type="text" className="form-control" name="planet-name" placeholder="Name of planet" onChange={(e) => this.setState({ name: e.target.value })} value={this.state.name}></input>
                    </div>
                    <div className="form-control">
                        <input type="text" className="form-control" name="planet-size" placeholder="Size of planet" onChange={(e) => this.setState({ size: e.target.value })} value={this.state.size}></input>
                    </div>
                    <div className="form-control">
                        <input type="text" className="form-control" name="planet-date-discovered" placeholder="Date discovered" onChange={(e) => this.setState({ date_discovered: e.target.value })} value={this.state.date_discovered}></input>
                    </div>

                    <button onClick={this.handleSubmit}>Submit</button>
                </form>
            </div>
        )
    }
}

export default Update;
