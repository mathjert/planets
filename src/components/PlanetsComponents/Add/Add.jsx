import React from 'react';
import Styles from './Add.module.css'


class Add extends React.Component {

    state = {
        name: '',
        size: '',
        date_discovered: '',
    }

    onSubmit(event) {
        event.preventDefault();
    }

    handleSubmit = () => {
        let { name, size, date_discovered } = this.state;
        if(!name || !size || !date_discovered) {
            console.log('here');
            alert('Cannot add planet without inputting all fields');
        }else {
            this.props.addToDatabase(this.state.name, this.state.size, this.state.date_discovered);
            this.setState({
                name: '',
                size: '',
                date_discovered: '',
            });
        }
       // this.props.addToDatabase(this.state.name, this.state.size, this.state.date_discovered);
        
    }

    handleChange(event){
        console.log(event);
        //const target = event.target;
        //const value = target.value;
        //const name = target.name;
        console.log('target: ' + event.target);
        console.log('value: ' + event.target.value);
        console.log('name: ' + event.target.value.name);
        //this.setState({
        //[name]: value
        //});
    }

    render() {
        const enabled = (this.state.name.length > 0 && this.state.size.length > 0 && this.state.date_discovered.length > 0);
        console.log(enabled);
        return (
            <div>
                <form onSubmit={this.onSubmit} className={Styles.planetForm}>
                    <div className="form-control">
                        <input type="text" className="form-control" name="planet-name" placeholder="Name of planet" onChange={(e) => this.setState({name: e.target.value})} value={this.state.name}></input>
                    </div>
                    <div className="form-control">
                        <input type="text" className="form-control" name="planet-size" placeholder="Size of planet" onChange={(e) => this.setState({size: e.target.value})} value={this.state.size}></input>
                    </div>
                    <div className="form-control">
                        <input type="text" className="form-control" name="planet-date-discovered" placeholder="Date discovered" onChange={(e) => this.setState({date_discovered: e.target.value})} value={this.state.date_discovered}></input>
                    </div>
    
                    <button onClick={this.handleSubmit} disabled={!enabled}>Submit</button>
                </form>
            </div>
        );
    }
}

export default Add;