import React from 'react';
import Styles from './SinglePlanet.module.css';

class SinglePlanet extends React.Component {
    
    state = {
        id: this.props.planetID,
        name: '',
        size: '',
        date_discovered: '',
    }

    componentDidMount(){
        this.setState({
            name: this.props.planet.name,
            size: this.props.planet.size,
            date_discovered: this.props.planet.date_discovered,
        });
    }

    handleSubmit(event) {
        event.preventDefault();
    }

    onSubmitHandle(){
        let {id, name, size, date_discovered} = this.state;
        this.props.updatePlanet(id, name, size, date_discovered);
    }
    
    
    render() {
        return (
            <div className={Styles.outer}>
                <div className={Styles.planet}>
                    <h4>{this.props.planet.name}</h4> <br />
                    size: {this.props.planet.size} <br />
                    DD: {this.props.planet.date_discovered}<br />
                    id: {this.props.planet.id}
                </div>
                <div className={Styles.overlay}>
                    <span>i</span>
                   <form onSubmit={this.handleSubmit}>
                       <div className={Styles.inputFieldDiv}>
                           <input className={Styles.inputField} type="text" placeholder="Planet name" value={this.state.name} onChange={(e) => this.setState({name: e.target.value})} />
                       </div>
                       <div className={Styles.inputFieldDiv}>
                           <input className={Styles.inputField} type="text" placeholder="Planet size" value={this.state.size} onChange={(e) => this.setState({size: e.target.value})} />
                       </div>
                       <div className={Styles.inputFieldDiv}>
                           <input className={Styles.inputField} type="text" placeholder="Date discovered" value={this.state.date_discovered} onChange={(e) => this.setState({date_discovered: e.target.value})} />
                       </div>
                       <button className={Styles.submitButton} onClick={() => this.onSubmitHandle()}>Submit changes</button>
                   </form>
                   <button className={Styles.deleteButton} onClick={() => this.props.deletePlanet(this.state.id)}>Delete this planet</button>
                </div>
                
            </div>
        );
    }
   
}

export default SinglePlanet;