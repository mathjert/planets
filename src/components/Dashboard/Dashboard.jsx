import React from 'react';
import Styles from './Dashboard.module.css';

const Dashboard = props => {
    return (
        <div className={Styles.headerimgdiv}>
            <img className={Styles.headerimg} src="https://www.nasa.gov/sites/default/files/thumbnails/image/pia21974.jpg" alt="" />
        </div>
    );
}

export default Dashboard;

