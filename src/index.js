import React from 'react';
import ReactDOM from 'react-dom';
import { BrowserRouter as Router, Route  } from 'react-router-dom';
import App from './App/App';
import './index.css';
import AllPlanets from './containers/planets/AllPlanets/AllPlanets';
import About from './containers/About/About';

const routing = (
    <Router>
        <div>
            <Route excact path="/" component={App} />
            <Route path="/about" component={About} />
            <Route path="/planets" component={AllPlanets} />
        </div>
    </Router>
);

ReactDOM.render(
    routing,
    document.getElementById('root'),
);