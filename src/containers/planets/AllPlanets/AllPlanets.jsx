import React from 'react';
import SinglePlanet from '../../../components/PlanetsComponents/SinglePlanet/SinglePlanet';
import Styles from './AllPlanets.module.css';
import Axios from 'axios';
import Add from '../../../components/PlanetsComponents/Add/Add';

class AllPlanets extends React.Component {

    state = {
        planets: [],
        addPlanetToggle: false,
       // header: {
        //    "Authorization": "Bearer ac52b6b3a6b004b3088e29c30a93acadc2b7b728e1e6e2efe88da735bc2cf0d0",
         //   "Content-Type": "application/json",
        //},
        header: {
            "Authorization": "Bearer 4e17403529c28e8718e21bb6b05aa7443bd0e6e6c9d83dd437e7c6df8908f037",
            "Content-Type": "application/json",

        },
        //url: 'http://localhost:5000/api/v1/planets',
        url: 'https://planetsapi-noroff.herokuapp.com/api/v1/planets',
        addExtention: '/add',
        updateExtention: '/update',
        deleteExtention: '/delete',
    }

    async componentDidMount() {
        try {
            const resp = await Axios({
                method: 'get',
                url: this.state.url,
                headers: this.state.header,
            }).then(resp => resp.data);
            this.setState({
                planets: resp,
            });
        } catch (e) {
            console.log(e);
        }
    }
    // alle på 
    async addToDatabase(name, size, date_discovered) {
        try {
            const resp = await Axios({
                method: 'post',
                url: this.state.url + this.state.addExtention,
                headers: this.state.header,
                data: {
                    planet: {
                        name: name,
                        size: size,
                        date_discovered: date_discovered
                    }
                }
            }).then(resp => resp.data);
            this.setState({
                planets: [
                    resp.planet[0],
                    ...this.state.planets,
                ]
            });
        } catch (e) {
            console.log(e);
        }
    }

    async updatePlanet(id, name, size, date_discovered) {
        try {
            const resp = await Axios({
                method: 'patch',
                url: this.state.url + this.state.updateExtention,
                headers: this.state.header,
                data: {
                    planet: {
                        id: id,
                        name: name,
                        size: size,
                        date_discovered: date_discovered
                    }
                }
            }).then(resp => resp.data);
            this.setState({
                planets: this.state.planets.map(planet => planet.id === resp.planet.id ? resp.planet : planet),
            });
        } catch (e) {
            console.log(e);
        }
    }

    async deletePlanet(planetid) {
        try {
            const resp = await Axios({
                method: 'delete',
                url: this.state.url + this.state.deleteExtention,
                headers: this.state.header,
                params: {
                    id: planetid
                }
            }).then(resp => {
                return resp.data;
            });
            if(resp.success) {
                this.setState({
                    planets: this.state.planets.filter(planet => planet.id !== planetid),
                });
            }
        } catch (e) {
            console.log(e);
        }
    }

    render() {
        const planetsOut = this.state.planets.map(planet => {
            return (
                <SinglePlanet planet={planet}
                    planetID={planet.id}
                    key={planet.id}
                    deletePlanet={(id) => this.deletePlanet(id)}
                    updatePlanet={(id, name, size, date_discovered) => this.updatePlanet(id, name, size, date_discovered)} />
            );
        });
        return (
            <React.Fragment>
                {this.state.addPlanetToggle ? 
                <div>
                    <div>
                    <Add addToDatabase={(name, size, date_discovered) => this.addToDatabase(name, size, date_discovered)}/>
                    </div>
                    <button className={Styles.addPlanetButton} onClick={() => this.setState({addPlanetToggle: !this.state.addPlanetToggle})}>
                        Done
                    </button>
                </div> 
                :
                <div>
                <button className={Styles.addPlanetButton} onClick={() => this.setState({addPlanetToggle: !this.state.addPlanetToggle})}>Add planet</button>
                </div>}
                <div className={Styles.planet}>
                    {planetsOut}
                </div>
            </React.Fragment>
        );
    }
}

export default AllPlanets;