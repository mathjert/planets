import React from 'react';
import Dashboard from '../../pages/Dashboard/Dashboard';

class Home extends React.Component {

    render(){
        return(
            <Dashboard />
        );
    }
}

export default Home;