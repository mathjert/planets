import React from 'react';

class About extends React.Component {

    render() {
        return (
            <div>
                <h3>Planets</h3>
                <p>
                    Planets is a task given in the Accelerate program at Noroff. <br/>
                    This task is the front-end for an api made earlier in the program. <br/>
                    You can find it <a href="https://planetsapi-noroff.herokuapp.com">here</a>. <br/>
                    <hr/>
                    This website let's you add, modify and delete planets from the API-s database. <br/>
                    To get started visit the Planets tab. To modify or delete an existing planet, hover <br/>
                    over the i in the upper right corner. This will bring up the menu to make modifications <br/>
                    or delete the planet. To add a planet, just press the add planet button.

                </p>
            </div>
        );
    }
}

export default About;