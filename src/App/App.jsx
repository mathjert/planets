import React from 'react';
import { Link } from 'react-router-dom';
import Styles from './App.module.css';
import Dashboard from '../components/Dashboard/Dashboard';

/* 
 * component. Code that can be reused. AKA smågreier som single planet 
 */

class App extends React.Component {

    state = {
        linkNotPressed: true,
    }

    setTrue() {
        this.setState({
            linkNotPressed: true,
        });
    }

    setFalse() {
        this.setState({
            linkNotPressed: false,
        });
        console.log(this.state.linkPressed);
    }

    render() {
        return (
            <React.Fragment>
                <div className={Styles.container}>
                    <div className={Styles.routingComponent}>
                        <Link to="/" className={Styles.logo} onClick={() => this.setTrue()}>Planets </Link>
                        <nav className={Styles.navList}>
                            <ul className={Styles.ulList}>
                                <li className={Styles.liItem}>
                                    <Link to="/" className={Styles.link} onClick={() => this.setTrue()}>Home</Link>
                                </li>
                                <li className={Styles.liItem}>
                                    <Link to="/about" className={Styles.link} onClick={() => this.setFalse()}>About</Link>
                                </li>
                                <li className={Styles.liItem}>
                                    <Link to="/planets" className={Styles.link} onClick={() => this.setFalse()}>Planets</Link>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
                    <Dashboard /> 
            </React.Fragment>
        );
    }
}

export default App;